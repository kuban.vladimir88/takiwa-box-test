import { Router } from 'express';
import auth from '../middleware/auth.js';
import boxController from '../controllers/box.controller.js';
const router = new Router();

router.post('/box', auth, boxController.createBox);
router.post('/box/:id', auth, boxController.uploadCSV);
router.get('/box', auth, boxController.getAllBoxes);
router.get('/box/:id', auth, boxController.getBoxbyId);
router.get('/download/:id', boxController.downloadCSV);
router.get('/public', boxController.getPublicBoxes);
router.put('/box/:id', auth, boxController.updateBox);
router.delete('/box/:id', auth, boxController.deleteBox);

export default router;
