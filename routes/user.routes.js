import { Router } from 'express';
import auth from '../middleware/auth.js';
import userController from '../controllers/user.controller.js';
const router = new Router();

router.post('/user/register', userController.userSignUp);
router.post('/user/login', userController.userLogin);
router.put('/user/:id', auth, userController.updateUser);

// *! router.get('/user/welcome', auth, userController.testJwt); test route delete later

export default router;
