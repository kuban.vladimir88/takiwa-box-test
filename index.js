import env from 'dotenv';
import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import fileUpload from 'express-fileupload';
import cors from 'cors';
import boxRouter from './routes/box.routes.js';
import userRouter from './routes/user.routes.js';

env.config();
const PORT = process.env.PORT || 3000;
const app = express();

//middleware
app.use(express.json());
app.use(cors({ origin: `${process.env.FRONTEND_APP}` }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('static'));
app.use(fileUpload({}));

//routes
app.use('/api', boxRouter);
app.use('/api', userRouter);
//app.use('/user', userRouter);

async function startApp() {
  try {
    await mongoose.connect(
      process.env.URL,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      },
      (err) => {
        if (err) {
          console.log(`error! ${err}`);
        }
      }
    );
    app.listen(PORT, () => console.log('Server works on ' + PORT));
  } catch (e) {
    console.log(e);
  }
}

startApp();
