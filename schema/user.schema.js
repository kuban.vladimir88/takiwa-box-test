import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

const UserSchema = new mongoose.Schema({
  username: { type: String, required: true, unique: true },
  email: { type: String, required: true, unique: true },
  password: { type: String },
  role: { type: String, default: 'penguin' },
  token: { type: String },
});

const Model = mongoose.model('user', UserSchema, 'User');

export default Model;
