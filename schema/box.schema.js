import mongoose from 'mongoose';

const BoxSchema = new mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String, required: true },
  owner: { type: String, required: true },
  shared: { type: Boolean, default: false },
  host: { type: String },
  filename: { type: String, default: 'none' },
  filelink: { type: String, default: 'none' },
  internal: { type: String, default: '' }, //if we host internally, to avoid filename duplicates- this refers to file path
});

const Model = mongoose.model('box', BoxSchema, 'Box');

export default Model;
