import UserService from '../services/user.service.js';

class UserController {
  async userSignUp(req, res) {
    if (!Object.keys(req.body).length) {
      res.status(400).json({
        message: 'Request body cannot be empty',
      });
    } else if (Object.keys(req.body).length < 3) {
      res.status(400).json({
        message: 'Not all fields are provided for registration',
      });
    } else {
      const { username, email, password } = req.body;
      try {
        const newUser = await UserService.createUser({
          username,
          email,
          password,
        });
        res.status(200).json(newUser);
      } catch (error) {
        res.status(405).json(`${error}`);
      }
    }
  }

  async userLogin(req, res) {
    try {
      const user = await UserService.loginUser(req.body);
      res.status(200).json(user);
    } catch (error) {
      res.status(401).json(`Unauthorized: ${error}`);
    }
  }

  async updateUser(req, res) {
    const { id } = req.params;
    try {
      const user = await UserService.updateUser(id, req.body);
      res.status(200).json(user);
    } catch (error) {
      res.status(500).json(error);
    }
  }

  async testJwt(req, res) {
    res.status(200).send('Welcome 🙌 ');
  }
}

export default new UserController();
