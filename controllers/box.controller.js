import BoxService from '../services/box.service.js';

class BoxController {
  async createBox(req, res) {
    if (!Object.keys(req.body).length) {
      res.status(400).json({
        message: 'Request body cannot be empty',
      });
    } else {
      const { name, description, owner } = req.body;
      if (name && description && owner) {
        try {
          const box = await BoxService.createBox(req.body);
          res.status(200).json(box);
        } catch (error) {
          res.status(500).json(`error: ${error}`);
        }
      } else {
        res.status(500).json('Make sure name, desc and owner are present');
      }
    }
  }

  async getPublicBoxes(req, res) {
    try {
      const boxes = await BoxService.getPublicBoxes();
      res.json(boxes);
    } catch (error) {
      res.status(502).json(error);
    }
  }

  async getAllBoxes(req, res) {
    const owner = req.query.owner;
    try {
      const boxes = await BoxService.getAllBoxes(owner);
      return res.json(boxes);
    } catch (error) {
      res.status(500).json(error);
    }
  }

  async getBoxbyId(req, res) {
    try {
      const box = await BoxService.getBoxbyId(req.params.id);
      return res.json(box);
    } catch (error) {
      res.status(500).json(error);
    }
  }

  async updateBox(req, res) {
    const { id } = req.params;
    try {
      const box = await BoxService.updateBox(id, req.body);
      res.status(200).json(box);
    } catch (error) {
      res.status(500).json(error.message);
    }
  }

  async uploadCSV(req, res) {
    const { id } = req.params;
    try {
      const box = await BoxService.uploadCSV(id, req.files.csvfile);
      res.json(box);
    } catch (error) {
      res.status(500).json(`Controller error: ${error}`);
    }
  }

  async downloadCSV(req, res) {
    const { id } = req.params;
    try {
      const box = await BoxService.getBoxbyId(id);
      if (box.internal) {
        const file = `static/${box.internal}`;
        res.download(file, box.filename);
      } else {
        res.status(400).json('no file in the box');
      }
    } catch (error) {
      res.status(500).json(`Controller error: ${error}`);
    }
  }

  async deleteBox(req, res) {
    const { id } = req.params;
    try {
      const result = await BoxService.deleteBox(id);
      res.status(200).json(result);
    } catch (error) {
      res.status(500).json(error.message);
    }
  }
}

export default new BoxController();
