import * as path from 'path';
import fs from 'fs';
import * as uuid from 'uuid';

class FileService {
  fileSave(file) {
    try {
      const fileName = uuid.v4() + '.csv';
      const filePath = path.resolve('static', fileName);
      file.mv(filePath);
      return { fileName: file.name, intenalName: fileName };
    } catch (e) {}
  }

  fileDelete(fileName) {
    if (!fileName) return;
    const filePath = path.resolve('static', fileName);
    fs.unlinkSync(filePath);
    return '';
  }
}

export default new FileService();
