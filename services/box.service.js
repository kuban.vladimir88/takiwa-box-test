import Box from '../schema/box.schema.js';
import FileService from './file.service.js';

class BoxService {
  async createBox(box) {
    try {
      const newBox = await Box.create({
        ...box,
      });
      if (box.shared) {
        await this.updateBox(newBox._id, { shared: true });
      }
    } catch (error) {
      throw new Error(`${error}`);
    }
    return 'box created!';
  }

  async getBoxbyId(id) {
    try {
      const box = await Box.findById(id);
      if (!id) {
        throw new Error('Box not specified');
      }
      return box;
    } catch (error) {
      throw new Error(`${error}`);
    }
  }

  async getAllBoxes(owner) {
    let boxes = [];
    try {
      boxes = await Box.find({ owner });
      if (!boxes) {
        throw new Error('boxes not found');
      }
      return boxes;
    } catch (error) {
      throw new Error(`${error}`);
    }
  }

  async getPublicBoxes() {
    try {
      const boxes = await Box.find({ shared: { $eq: true } });
      return boxes;
    } catch (error) {
      throw new Error(`${error}`);
    }
  }

  async updateBox(id, box) {
    if (!id) {
      throw new Error('id not specified');
    }

    const updatedBox = await Box.findByIdAndUpdate(id, box, { new: true });
    return updatedBox;
  }

  async uploadCSV(id, file) {
    const box = await this.getBoxbyId(id);
    if (box.filename !== 'none') {
      try {
        box.internal = FileService.fileDelete(box.internal);
        box.host = '';
        box.filelink = '';
        box.filename = 'none';
        box.internal = '';
      } catch (error) {
        throw new Error(`filedelete error: ${error}`);
      }
    }
    try {
      const uploadFile = FileService.fileSave(file);
      box.host = 'internal';
      box.filelink = 'host';
      box.filename = uploadFile.fileName;
      box.internal = uploadFile.intenalName;
      const updatedBox = await this.updateBox(id, box);
      return updatedBox;
    } catch (error) {
      throw new Error(`file upload error: ${error}`);
    }
  }

  async deleteBox(_id) {
    const result = await Box.deleteOne({ _id });
    if (result.deletedCount === 1) {
      return 'Box deleted';
    } else {
      throw new Error(`Error: can't delete the box with id: $id}`);
    }
  }
}

export default new BoxService();
