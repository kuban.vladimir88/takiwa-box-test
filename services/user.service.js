import User from '../schema/user.schema.js';
import bcrypt from 'bcrypt';
const salt = bcrypt.genSaltSync(10);
import jwt from 'jsonwebtoken';
class UserService {
  async createUser(user) {
    const { username, email, password } = user;
    const filterEmail = email.toLowerCase(); //sanitize email string to be safe
    const oldUser = await User.findOne({ email: filterEmail });

    if (oldUser) {
      throw new Error('User Already Exist. Please Login');
    }

    const token = jwt.sign(
      { username, email: filterEmail },
      process.env.TOKEN_KEY,
      {
        expiresIn: '2h',
      }
    );

    const hash = bcrypt.hashSync(password, salt);
    try {
      const user = await User.create({
        username,
        email,
        password: hash,
        role: 'penguin',
        token,
      });

      return user;
    } catch (error) {
      throw new Error(`${error}`);
    }
  }

  async getUserbyId(id) {
    try {
      const user = await User.findById(id);
      if (!id) {
        throw new Error('Box not specified');
      }
      return user;
    } catch (error) {
      throw new Error(`${error}`);
    }
  }
  async loginUser(user) {
    const { password, email: emailEntry } = user;
    const filterEmail = emailEntry.toLowerCase(); //sanitize email string to be safe
    const foundUser = await User.findOne({ email: filterEmail });
    if (foundUser) {
      const match = await bcrypt.compare(password, foundUser.password);

      if (match) {
        const { _id: id, username, role, email } = foundUser;
        const token = jwt.sign({ username, email }, process.env.TOKEN_KEY, {
          expiresIn: '2h',
        });
        foundUser.token = token;
        return { id, username, role, email, token };
      } else {
        throw new Error('Unauthorized');
      }
    } else {
      throw new Error('User with this email not found');
    }
  }

  async updateUser(id, user) {
    const { username, email, password } = user;
    if (!id) {
      throw new Error('id not specified');
    }

    const updatedUser = await User.findByIdAndUpdate(
      id,
      {
        username,
        email,
        password,
      },
      { new: true }
    );
    return updatedUser;
  }
}

export default new UserService();
