#File Boxes app backend

This project was completed as a tesk task.

### After you clone the project:

Create .env file and configure following variables:
URL= <your mongodb path>
PORT=<local port to run backend on>
FRONTEND_APP=<local port to run frontend on>
TOKEN_KEY=<JWT secret>>

As an example result .env file would look like this:
URL=mongodb+srv://<mongodbUsername>:<mongodbPassword>@cluster0.cctbj.mongodb.net/<DBname>?retryWrites=true&w=majority
PORT=3008
FRONTEND_APP=http://localhost:3000
TOKEN_KEY=fileboxes098

#### Run The App Locally

```sh
npm run install && npm start
```
